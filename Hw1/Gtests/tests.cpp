#include "gtest/gtest.h"

extern "C" {
  #include "ticketsOperations.h"
  #include "actions.h"
}

AirTicket expectedTickets[] {
    {"AI2", "Внуково", "Пулково", 180, 4000},
    {"B21", "Домодедово", "Хитроу", 400, 8000},
    {"B12", "Внуково", "Даллас", 1000, 10000},
    {"P32", "Ханеда", "Хартфилд", 830, 3001},
    {"P33", "Внуково", "Даллас", 800, 10000},
    {"B11", "Внуково", "Даллас", 810, 9999},
    {"P39", "Ханеда", "Хартфилд", 830, 3100}
};


TEST(loadData, wrongPath) {
  int receivedCount = 0;
  int expectedCount = 0;
  ASSERT_FALSE(loadData(&receivedCount, "path"));

  ASSERT_EQ(receivedCount, expectedCount);
}

TEST(loadData, emptyFile) {
  int receivedCount = 0;
  int expectedCount = 0;
  ASSERT_FALSE(loadData(&receivedCount, "../Gtests/empty.dat"));

  ASSERT_EQ(receivedCount, expectedCount);
}


TEST(loadData, correctPath) {
  int receivedCount = 0;
  int expectedCount = 7;

  AirTicket* receivedTickets = loadData(&receivedCount, "../Gtests/ticketsTest.dat");

  ASSERT_EQ(receivedCount, expectedCount);

  for (int i = 0; i < receivedCount; i++) {
    ASSERT_STREQ(receivedTickets[i].code, expectedTickets[i].code);
    ASSERT_STREQ(receivedTickets[i].arrivAirport, expectedTickets[i].arrivAirport);
    ASSERT_STREQ(receivedTickets[i].departAirport, expectedTickets[i].departAirport);

    ASSERT_EQ(receivedTickets[i].time, expectedTickets[i].time);
    ASSERT_EQ(receivedTickets[i].cost, expectedTickets[i].cost);
  }

  free(receivedTickets);
}

TEST(loadTicketsDataFromFile, incorrectFile) {
  int receivedCount = 0;
  int expectedCount = 0;
  FILE* file = NULL;
  ASSERT_FALSE(loadTicketsDataFromFile(file, &receivedCount));

  ASSERT_EQ(receivedCount, expectedCount);
}

TEST(loadTicketsDataFromFile, emptyFile) {
  int receivedCount = 0;
  int expectedCount = 0;

  FILE* ftickets = fopen("../Gtests/empty.dat", "r");
  ASSERT_TRUE(ftickets);

  ASSERT_FALSE(loadTicketsDataFromFile(ftickets, &receivedCount));
  ASSERT_EQ(receivedCount, expectedCount);

  fclose(ftickets);
}

TEST(loadTicketsDataFromFile, correctFile) {
  int receivedCount = 0;
  int expectedCount = 7;

  FILE* ftickets = fopen("../Gtests/ticketsTest.dat", "r");
  ASSERT_TRUE(ftickets);

  AirTicket* receivedTickets = loadTicketsDataFromFile(ftickets, &receivedCount);
  fclose(ftickets);

  ASSERT_EQ(receivedCount, expectedCount);

  for (int i = 0; i < receivedCount; i++) {
    ASSERT_STREQ(receivedTickets[i].code, expectedTickets[i].code);
    ASSERT_STREQ(receivedTickets[i].arrivAirport, expectedTickets[i].arrivAirport);
    ASSERT_STREQ(receivedTickets[i].departAirport, expectedTickets[i].departAirport);

    ASSERT_EQ(receivedTickets[i].time, expectedTickets[i].time);
    ASSERT_EQ(receivedTickets[i].cost, expectedTickets[i].cost);
  }

  free(receivedTickets);
}


TEST(findMostOptimalFlight, noData) {
  AirTicket expectedTicket = {"None"};
  AirTicket* tickets = NULL;
  int ticketsCount = 7;
  char dep[] = "Внуково";
  char arr[] = "Пулково";
  int sort = 0;
  AirTicket receivedTicket = findMostOptimalFlight(tickets, ticketsCount,
       dep, arr, sort);
  ASSERT_STREQ(receivedTicket.code, expectedTicket.code);
}

TEST(findMostOptimalFlight, missingTicket) {
  AirTicket expectedTicket = {"None"};
  int ticketsCount = 7;
  char dep[] = "Внуково";
  char arr[] = "Марс";
  int sort = 0;
  AirTicket receivedTicket = findMostOptimalFlight(expectedTickets, ticketsCount,
                                                   dep, arr, sort);
  ASSERT_STREQ(receivedTicket.code, expectedTicket.code);
}

TEST(findMostOptimalFlight, incorrectSortOption) {
  AirTicket expectedTicket = {"P33", "Внуково", "Даллас", 800, 10000};
  int ticketsCount = 7;
  char dep[] = "Внуково";
  char arr[] = "Даллас";
  int sort = 10;
  AirTicket receivedTicket = findMostOptimalFlight(expectedTickets, ticketsCount,
                                                   dep, arr, sort);
  ASSERT_STREQ(receivedTicket.code, expectedTicket.code);
  ASSERT_STREQ(receivedTicket.departAirport, expectedTicket.departAirport);
  ASSERT_STREQ(receivedTicket.arrivAirport, expectedTicket.arrivAirport);

  ASSERT_EQ(receivedTicket.cost, expectedTicket.cost);
  ASSERT_EQ(receivedTicket.time, expectedTicket.time);
}

TEST(findMostOptimalFlight, costSort) {
  AirTicket expectedTicket = {"B11", "Внуково", "Даллас", 810, 9999};
  int ticketsCount = 7;
  char dep[] = "Внуково";
  char arr[] = "Даллас";
  int sort = 1;
  AirTicket receivedTicket = findMostOptimalFlight(expectedTickets, ticketsCount,
                                                   dep, arr, sort);
  ASSERT_STREQ(receivedTicket.code, expectedTicket.code);
  ASSERT_STREQ(receivedTicket.departAirport, expectedTicket.departAirport);
  ASSERT_STREQ(receivedTicket.arrivAirport, expectedTicket.arrivAirport);

  ASSERT_EQ(receivedTicket.cost, expectedTicket.cost);
  ASSERT_EQ(receivedTicket.time, expectedTicket.time);
}

TEST(findMostOptimalFlight, timeSort) {
  AirTicket expectedTicket = {"P33", "Внуково", "Даллас", 800, 10000};
  int ticketsCount = 7;
  char dep[] = "Внуково";
  char arr[] = "Даллас";
  int sort = 0;
  AirTicket receivedTicket = findMostOptimalFlight(expectedTickets, ticketsCount,
                                                   dep, arr, sort);
  ASSERT_STREQ(receivedTicket.code, expectedTicket.code);
  ASSERT_STREQ(receivedTicket.departAirport, expectedTicket.departAirport);
  ASSERT_STREQ(receivedTicket.arrivAirport, expectedTicket.arrivAirport);

  ASSERT_EQ(receivedTicket.cost, expectedTicket.cost);
  ASSERT_EQ(receivedTicket.time, expectedTicket.time);
}


int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
