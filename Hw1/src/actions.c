#include "actions.h"

AirTicket* loadData(int* ticketsCount, const char* filePath ) {
  FILE* ftickets = fopen(filePath, "r");
  if (!ftickets) {
    puts("Not access to data");
    return NULL;
  }
  AirTicket* tickets = loadTicketsDataFromFile(ftickets, ticketsCount);
  fclose(ftickets);
  if (!tickets) {
    return NULL;
  }
  puts("Successful");
  return tickets;
}


void showMostOptimalFlight(AirTicket* tickets, int ticketsCount) {
  if (!tickets) {
    puts("No Data");
    return;
  }
  char dep[30], arr[30];
  puts("Input Departure and arrival airports.");
  scanf("%29s%29s", dep, arr);
  scanf("%*[^\n]");
  puts("sorted by time : 0 \nsorted by cost: 1");
  int sortParam = 0;
  scanf("%d", &sortParam);
  scanf("%*[^\n]");
  AirTicket bestTicket = findMostOptimalFlight(tickets, ticketsCount,
                                               dep, arr, sortParam);

  if (!strcmp(bestTicket.code, "None")) {
    puts("The ticket does not exist according to the entered data");
  } else {
    printf("%s", bestTicket.code);
    printf(" %s", bestTicket.departAirport);
    printf(" %s", bestTicket.arrivAirport);
    printf(" %d", bestTicket.time);
    printf(" %d\n", bestTicket.cost);
  }
}
