#include "ticketsOperations.h"

AirTicket* loadTicketsDataFromFile(FILE* ftickets, int* ticketsCount) {
  if (!ftickets) {
    return NULL;
  }
  int capacity = 5;
  *ticketsCount = 0;

  AirTicket* tickets = (AirTicket*)malloc(sizeof(AirTicket) * capacity);
  if (!tickets) {
    return NULL;
  }

  while (fscanf(ftickets, "%10s%29s%29s%d%d",
                tickets[*ticketsCount].code,
                tickets[*ticketsCount].departAirport,
                tickets[*ticketsCount].arrivAirport,
                &tickets[*ticketsCount].time,
                &tickets[*ticketsCount].cost) == 5) {
    (*ticketsCount)++;
    if ((*ticketsCount) >= capacity) {
      capacity*= 2;
      AirTicket* buf = (AirTicket*)realloc(tickets,
              sizeof(AirTicket) * capacity);
      if (!buf) {
        free(tickets);
        return NULL;
      } else {
        tickets = buf;
      }
    }
  }
  if ((*ticketsCount) == 0) {
    free(tickets);
    tickets = NULL;
  }
    return tickets;
}


AirTicket findMostOptimalFlight(const AirTicket* tickets, int ticketsCount,
                                const char* dep, const char* arr, int option) {
  AirTicket bestFlight = {"None"};
  if (!tickets) {
    return bestFlight;
  }
  for (int i = 0; i < ticketsCount; i++) {
    if (!strcmp(tickets[i].departAirport, dep)
        && (!strcmp(tickets[i].arrivAirport, arr))) {
      if (bestFlight.time == 0) {
        bestFlight = tickets[i];
        continue;
      }

      switch (option) {
      case TIME:
        if (tickets[i].time < bestFlight.time) {
          bestFlight = tickets[i];
        }
        break;

      case COST:
        if (tickets[i].cost < bestFlight.cost) {
          bestFlight = tickets[i];
        }
        break;

      default:
        if (tickets[i].time < bestFlight.time) {
          bestFlight = tickets[i];
        }
        break;
      }
    }
  }
  return  bestFlight;
}
