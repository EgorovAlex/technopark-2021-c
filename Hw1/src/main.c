#include "actions.h"
#include "airTicket.h"

int main() {
  char filePath[] = TICKETS_PATH;
  AirTicket* tickets = NULL;
  int ticketsCount = 0;
  int action = 0;
  puts("To load the data, press 0\nTo find the optimal flight, press 1");

  while (scanf("%d", &action) == 1) {
    switch (action) {
    case 0:
      if (tickets) {
        free(tickets);
      }
      tickets = loadData(&ticketsCount, filePath);
      break;
    case 1:
      showMostOptimalFlight(tickets, ticketsCount);
      break;
    default:
      puts("Input correct action");
      break;
    }
    puts("To load the data, press 0\nTo find the optimal flight, press 1");
  }
  if (tickets) {
    free(tickets);
  }
  return 0;
}
