#!/usr/bin/env bash

set -e
set -o pipefail

function print_header() {
    echo -e "\n***** ${1} *****"
}



print_header "RUN cpplint.py"
python2.7 Hw1/linters/cpplint/cpplint.py --extensions=c Hw1/include/* Hw1/src/* 
python2.7 Hw1/linters/cpplint/cpplint.py Hw1/Gtests/tests.cpp

print_header "RUN cppcheck"
if [ "${1}" == "--local" ]; then
	CPPCHECK="cppcheck"
else
	CPPCHECK="Hw1/linters/cppcheck/cppcheck"
fi
${CPPCHECK} Hw1/src/* Hw1/include/* Hw1/Gtests/tests.cpp --enable=all --error-exitcode=1 -I include --suppress=missingIncludeSystem



print_header "SUCCESS"
