#ifndef HW1_INCLUDE_ACTIONS_H_
#define HW1_INCLUDE_ACTIONS_H_

#include "ticketsOperations.h"
AirTicket* loadData(int* ticketsCount,  const char* filePath);

void showMostOptimalFlight(AirTicket* tickets, int ticketsCount);

#endif  // HW1_INCLUDE_ACTIONS_H_
