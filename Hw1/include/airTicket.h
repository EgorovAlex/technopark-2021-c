#ifndef HW1_INCLUDE_AIRTICKET_H_
#define HW1_INCLUDE_AIRTICKET_H_

typedef struct {
  char code[5];
  char departAirport[30];
  char arrivAirport[30];
  int time;
  int cost;
} AirTicket;

#endif  // HW1_INCLUDE_AIRTICKET_H_
