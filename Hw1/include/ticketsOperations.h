#ifndef HW1_INCLUDE_TICKETSOPERATIONS_H_
#define HW1_INCLUDE_TICKETSOPERATIONS_H_
#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include "airTicket.h"
#include "defines.h"

AirTicket* loadTicketsDataFromFile(FILE* ftickets, int* ticketsCount);

AirTicket findMostOptimalFlight(const AirTicket* tickets, int ticketsCount,
                                const char* dep, const char* arr, int option);


#endif  // HW1_INCLUDE_TICKETSOPERATIONS_H_
